import QtQuick 2.0
import Sailfish.Silica 1.0

/***
  Элемент всплывающего окна для выбора 1 элемента
  Для правильной работы необходимо разместить последним элементов на Page

  Для работы необходимо передать в popupModel список элементов для выбора,
  а в viewName указать поле, текст которого будет отображаться в СhoisePopup

  Сигнал enterChoise нужен для получения индекса выбранного в окне элемента
  ***/

ContainerPopup {
    property var popupModel
    property string viewName: "name"
    property var selectedIndex
    property string titlePopup

    id: choisePopup

    popHeight: 300
    popWidth: choisePopup.width - choisePopup.width*0.2
    property real heighItems
    property real heighButton: 70

    signal enterChoise(var index);

    ListModel{
        id: checkListModel;
        dynamicRoles: true
    }
    function clearChecked(){
        for(var i = 0; i < checkListModel.count; i++){
            checkListModel.setProperty(i, "checked", false);
        }
    }

    onVisibleContainerChanged: {
        if(!stateView){
            //checkListModel.clear();
        }
    }

    view: Column{
        property var selectModel;
        property var selectIndex;
        id: choiseColumn
        anchors.fill: parent
        Rectangle {
            color: "transparent"
            height: 20
            width: 1
        }
        Label{
            id: titleLabel
            anchors.horizontalCenter: parent.horizontalCenter
            text: titlePopup
        }
        SilicaListView {
            id: view
            width: popWidth
            model: checkListModel
            clip: true

            delegate: TextSwitch {
                text: model.text;
                checked: model.checked
                automaticCheck: false
                _label.wrapMode: Text.WordWrap
                onClicked: {
                    if(checked===false){
                        clearChecked();
                        model.checked=true;
                        selectModel = model;
                        selectIndex = model.index;
                    }
                }
            }

            footer: Rectangle {
                color: "transparent"
                height: Theme.paddingMedium
                width: parent.width
            }
            onContentHeightChanged: {
                height = contentHeight;
                choisePopup.popHeight = contentHeight + heighButton + 100;
            }
        }
        Button{
            id: selectButton
            text: "Выбрать"
            anchors.right: parent.right
            anchors.rightMargin: 15
            onClicked: {
                selectedIndex = selectIndex;
                enterChoise(selectedIndex);
                hide();
            }
            Component.onCompleted: {
                heighButton = height;
            }
        }
    }
    onPopupModelChanged: {
        checkListModel.clear();
        var i  = 0;
        for(var it in popupModel){
            var item = popupModel[it];
            checkListModel.append({
                                      "text": item[viewName],
                                      "index": i,
                                      "checked": false
                                  });
            i+=1;
        }
        if(checkListModel.count)
        {
            checkListModel.setProperty(0, "checked", true);
            enterChoise(0);
        }
    }
}
