import QtQuick 2.0
import QtGraphicalEffects 1.0
import Sailfish.Silica 1.0

ContainerPopup {
    id: syncPopup

    property string viewName: "name"
    property var selectedIndex
    property string titlePopup

    property string coverInfoText
    property var coverInfoList: ["confirm", "busy", "complite", "error"]

    property var titleLabel
    property var titleText

    signal syncStart
    signal syncError(string failText)
    signal syncFinish 

    property string errorText

    Loader{
        id: loaderHeader

        anchors.left: parent.left
        anchors.right: parent.right

        sourceComponent: Label{
            id: syncTitle
            width: implicitWidth
            horizontalAlignment: "AlignHCenter"
            text: titleText
            font.pixelSize: Theme.fontSizeMedium
            font.bold: true
            wrapMode: Label.WordWrap
            Component.onCompleted: {
                syncPopup.titleLabel = syncTitle;
            }
        }

        onParentChanged: {
            item.width = parent.width;
        }
    }
    Loader{
        id: loaderView
        anchors{
            left: parent.left
            right: parent.right
        }

        onHeightChanged: {
            var h = loaderHeader.height + loaderView.height + 50;
            syncPopup.popHeight = h;
        }
    }

    property Component currentView
    property Component confirmView
    property Component busyView: BackgroundItem{
        id: busyContainer
        height: busyIndicator.height
        BusyIndicator {
            id: busyIndicator
            size: BusyIndicatorSize.Large
            running: true

            onParentChanged: {
                anchors.horizontalCenter = parent.horizontalCenter;
            }
        }
    }

    property Component errorView: Column{
        id: errorContainter
        spacing: 10
        Label{
            anchors{
                left: parent.left
                leftMargin: 10
                right: parent.right
                rightMargin: 10
            }
            text: "Возникла ошибка при синхронизации"
            font.bold: true
            wrapMode: "WordWrap"
        }
        Label{
            anchors{
                left: parent.left
                leftMargin: 10
                right: parent.right
                rightMargin: 10
            }
            text: syncPopup.errorText
            wrapMode: "WordWrap"
        }
        Button{
            anchors.horizontalCenter: parent.horizontalCenter
            text: "ok"
            onClicked: {
                syncPopup.hide();
            }
        }
    }
    property Component compliteView: Column{
        id: comliteContainter
        spacing: 10
        Label{
            anchors{
                left: parent.left
                leftMargin: 10
                right: parent.right
                rightMargin: 10
            }
            text: "Отправка данных успешно завершена"
            wrapMode: "WordWrap"
        }
        Button{
            anchors.horizontalCenter: parent.horizontalCenter
            text: "ok"
            onClicked: {
                syncPopup.hide();
            }
        }
    }

    view: Column{
        id: syncView
        anchors{
            horizontalCenter: parent.horizontalCenter
            left: parent.left
            right: parent.right
        }

        spacing: 20

        children: [
            loaderHeader,
            loaderView
        ]
    }

    states: [
        State {
            name: "Confirm"
            PropertyChanges {
                target: loaderView
                sourceComponent: confirmView
            }
            PropertyChanges {
                target: syncPopup
                enabled: true
            }
        },
        State {
            name: "Busy"
            PropertyChanges {
                target: loaderView
                sourceComponent: busyView
            }
            PropertyChanges {
                target: syncPopup
                enabled: false
            }
        },
        State {
            name: "Complite"
            PropertyChanges {
                target: loaderView
                sourceComponent: compliteView
            }
        },
        State {
            name: "Error"
            PropertyChanges {
                target: loaderView
                sourceComponent: errorView
            }
            PropertyChanges {
                target: syncPopup
                enabled: true
            }
        },
        State{
            name: "HideView"
        }

    ]

    Component.onCompleted: {
        state = "HideView";
        coverInfoText = "";
    }

    onShowPopup: {
        state = "Confirm";
        coverInfoText = coverInfoList[0];
    }

    onSyncStart: {
        state = "Busy";
        coverInfoText = coverInfoList[1];
    }

    onSyncFinish: {
        state = "Complite";
        coverInfoText = coverInfoList[2];
    }

    onSyncError:{
        syncPopup.errorText = failText;
        state = "Error";
        coverInfoText = coverInfoList[3];
    }

}
