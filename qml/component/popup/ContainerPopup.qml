import QtQuick 2.5
import QtGraphicalEffects 1.0
import Sailfish.Silica 1.0

// Контейнер Popup для размещения в view компонента для отображения
// размер Popup берется из размера элемента во view

BackgroundItem {
    id: popupContainer

    property real popHeight: parent.height * 0.8
    property real popWidth: parent.width * 0.9
    property Component view;

    anchors.fill: parent
    visible: false
    enabled: true

    signal visibleContainerChanged(bool stateView);
    signal showPopup
    signal hidePopup

    function show(){
        popupContainer.visible=true;
        visibleContainerChanged(true);
        showPopup();
    }
    function hide(){
        popupContainer.visible=false;
        visibleContainerChanged(false);
        hidePopup();
    }

    Rectangle{
        anchors.fill: parent
        color: "#66000000"
    }
    MouseArea{
        id: closeArea
        anchors.fill: parent
        onClicked: {
            if(popupContainer.enabled) hide();
        }
    }

    Rectangle {
        id: workArea
        anchors.centerIn: parent
        height: popupContainer.popHeight
        width: popupContainer.popWidth
        //clip: flickable.contentHeight > height
        border.color: "red"
        border.width:  3
        radius: 30
        MouseArea{
            anchors.fill: parent
        }
        Loader{
            id: workContainer
            anchors.fill: workArea
            sourceComponent: view
        }

    }
}

/*##^##
Designer {
    D{i:0;formeditorZoom:1.33;height:310;width:310}
}
##^##*/
