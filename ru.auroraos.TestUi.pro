TARGET = ru.auroraos.TestUi

CONFIG += sailfishapp
CONFIG += qml_debug

PKGCONFIG += \

SOURCES += \
    src/main.cpp \

HEADERS += \

DISTFILES += \
    $$files(qml/*.qml) \
    $$files(qml/component/*.qml) \
    $$files(qml/component/popup/*.qml) \
    $$files(qml/pages/*.qml) \
    $$files(qml/cover/*.qml)

DISTFILES += \
    rpm/ru.auroraos.TestUi.spec \
    AUTHORS.md \
    CODE_OF_CONDUCT.md \
    CONTRIBUTING.md \
    LICENSE.BSD-3-CLAUSE.md \
    README.md \

SAILFISHAPP_ICONS = 86x86 108x108 128x128 256x256

CONFIG += auroraapp_i18n

TRANSLATIONS += \
    translations/ru.auroraos.TestUi.ts \
    translations/ru.auroraos.TestUi-ru.ts \
